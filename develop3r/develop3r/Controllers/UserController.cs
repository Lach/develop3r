﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using develop3r.DataAccess;
using develop3r.Models;

namespace develop3r.Controllers
{
    public class UserController : Controller
    {
        private UserRepository userRepo;

        public UserController()
        {
            userRepo = new UserRepository();
        }

        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult GetUsers()
        {
            var users = userRepo.GetAllUsers();
            return View(users);
        }

        [HttpGet]
        public ActionResult AddNewUser()
        {
            return View();
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult AddNewUser(UserModel newUser)
        {
            if (ModelState.IsValid)
            {
                const int id = 1;
                const string password = "password";

                // retreive the users Id once they have been added.  If it failed a zero will be returned.
                int result = userRepo.AddNewUser(id, newUser.FirstName, newUser.LastName, newUser.EmailAddress);

                if (result <= 0)
                {
                    TempData["ValidationMessage"] = "Add a new user failed";
                    return RedirectToAction("GetUsers");
                }
                if (result >= 1)
                {
                    return RedirectToAction("GetUsers");
                }
            }
            return View("Index");
        }

        [HttpGet]
        public ActionResult EditUser(int id)
        {
            if (id <= 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var user = userRepo.SearchUserById(id);
            if (user == null)
            {
                return HttpNotFound();
            }

            return View(user);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult EditUser(UserModel user)
        {
            if (ModelState.IsValid)
            {
                bool userId = userRepo.EditUser(user);
                if (!userId)
                {
                    TempData["ValidationMessage"] = "Edit user failed";
                    return RedirectToAction("GetUsers");
                }
                return RedirectToAction("GetUsers");
            }
            return RedirectToAction("GetUsers");
        }

        [HttpGet]
        public ActionResult DeleteUser(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);   
            }
            var user = userRepo.GetUserDetails();
            if (user == null)
            {
                return HttpNotFound();
            }
            return View(user);
        }

        [HttpPost, ValidateAntiForgeryToken]
        public ActionResult DeleteUser(UserModel user)
        {
            if (ModelState.IsValid)
            {
                var result = userRepo.DeleteUser(user);
                if (!result)
                {
                    TempData["ValidationMessage"] = "An error occured.  Unable to delete user";
                    return RedirectToAction("GetUsers");
                }
            }
            return RedirectToAction("GetUsers");
        }
    }
}