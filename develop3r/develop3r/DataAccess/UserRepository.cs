﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text.RegularExpressions;
using System.Web.ModelBinding;
using develop3r.Models;

namespace develop3r.DataAccess
{
    public class UserRepository
    {
        // Get a list of users
        public List<UserModel> GetAllUsers()
        {
            List<UserModel> listOfUsers = new List<UserModel>();

            var users = GetUserDetails();

            listOfUsers.Add(users);

            return listOfUsers;
        } 


 // Save User Info to text file and return an integer value for successful save
        public int AddNewUser(int id, string firstName, string lastName, string emailAddress)
        {
            try
            {
                StreamWriter swId = new StreamWriter("C:/myGit/develop3r/develop3r/develop3r/DataForTesting/UserDetails/Id.txt");
                StreamWriter swFirstName = new StreamWriter("C:/myGit/develop3r/develop3r/develop3r/DataForTesting/UserDetails/FirstName.txt");
                StreamWriter swLastName = new StreamWriter("C:/myGit/develop3r/develop3r/develop3r/DataForTesting/UserDetails/LastName.txt");
                StreamWriter swEmailAddress = new StreamWriter("C:/myGit/develop3r/develop3r/develop3r/DataForTesting/UserDetails/EmailAddress.txt");

                swId.WriteLine(id);
                swId.Close();
                swFirstName.WriteLine(firstName);
                swFirstName.Close();
                swLastName.WriteLine(lastName);
                swLastName.Close();
                swEmailAddress.WriteLine(emailAddress);
                swEmailAddress.Close();

            }
            catch (Exception e)
            {
                return 0;
            }

            return id;
        }
        
        public UserModel GetUserDetails()
        {
            int id = 0;
            string firstName = "";
            string lastName = "";
            string emailAddress = "";

            try
            {
                StreamReader srId = new StreamReader("C:/myGit/develop3r/develop3r/develop3r/DataForTesting/UserDetails/Id.txt");
                StreamReader srFirstName = new StreamReader("C:/myGit/develop3r/develop3r/develop3r/DataForTesting/UserDetails/FirstName.txt");
                StreamReader srLastName = new StreamReader("C:/myGit/develop3r/develop3r/develop3r/DataForTesting/UserDetails/LastName.txt");
                StreamReader srEmailAddress = new StreamReader("C:/myGit/develop3r/develop3r/develop3r/DataForTesting/UserDetails/EmailAddress.txt");

                int lineId = Convert.ToInt32(srId.ReadLine());
                string lineFirstName = srFirstName.ReadLine();
                string lineLastName = srLastName.ReadLine();
                string lineEmailAddress = srEmailAddress.ReadLine();

                // TODO Fix this loop
                while (lineFirstName != null && lineLastName != null && lineEmailAddress != null)
                {
                    id = lineId;
                    lineId = Convert.ToInt32(srId.ReadLine());
                    firstName = lineFirstName;
                    lineFirstName = srFirstName.ReadLine();
                    lastName = lineLastName;
                    lineLastName = srLastName.ReadLine();
                    emailAddress = lineEmailAddress;
                    lineEmailAddress = srEmailAddress.ReadLine();
                }

                srId.Close();
                srFirstName.Close();
                srLastName.Close();
                srEmailAddress.Close();

            }
            catch (Exception e)
            {
                // TODO Add error message
            }

            UserModel newUser = new UserModel(id, firstName, lastName, emailAddress);

            return newUser;
        }



// Search for users by their Id's
        public UserModel SearchUserById(int id)
        {
            UserModel userFound = GetUserDetails();

            if (userFound.Id == id)
            {
                return userFound;
            }
            else
            {
                return null;
            }
        }

// Edit users information
        public bool EditUser(UserModel user)
        {
            int id = user.Id;
            string firstName = user.FirstName;
            string lastName = user.LastName;
            string emailAddress = user.EmailAddress;
            
            try
            {
                StreamWriter swId = new StreamWriter("C:/myGit/develop3r/develop3r/develop3r/DataForTesting/UserDetails/Id.txt");
                StreamWriter swFirstName = new StreamWriter("C:/myGit/develop3r/develop3r/develop3r/DataForTesting/UserDetails/FirstName.txt");
                StreamWriter swLastName = new StreamWriter("C:/myGit/develop3r/develop3r/develop3r/DataForTesting/UserDetails/LastName.txt");
                StreamWriter swEmailAddress = new StreamWriter("C:/myGit/develop3r/develop3r/develop3r/DataForTesting/UserDetails/EmailAddress.txt");

                swId.WriteLine(id);
                swId.Close();
                swFirstName.WriteLine(firstName);
                swFirstName.Close();
                swLastName.WriteLine(lastName);
                swLastName.Close();
                swEmailAddress.WriteLine(emailAddress);
                swEmailAddress.Close();

            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

        public bool DeleteUser(UserModel user)
        {
            const int id = 0;
            const string firstName = null;
            const string lastName = null;
            const string emailAddress = null;

            try
            {
                StreamWriter swId = new StreamWriter("C:/myGit/develop3r/develop3r/develop3r/DataForTesting/UserDetails/Id.txt");
                StreamWriter swFirstName = new StreamWriter("C:/myGit/develop3r/develop3r/develop3r/DataForTesting/UserDetails/FirstName.txt");
                StreamWriter swLastName = new StreamWriter("C:/myGit/develop3r/develop3r/develop3r/DataForTesting/UserDetails/LastName.txt");
                StreamWriter swEmailAddress = new StreamWriter("C:/myGit/develop3r/develop3r/develop3r/DataForTesting/UserDetails/EmailAddress.txt");

                swId.WriteLine(id);
                swId.Close();
                swFirstName.WriteLine(firstName);
                swFirstName.Close();
                swLastName.WriteLine(lastName);
                swLastName.Close();
                swEmailAddress.WriteLine(emailAddress);
                swEmailAddress.Close();

            }
            catch (Exception e)
            {
                return false;
            }
            return true;
        }

    }
}