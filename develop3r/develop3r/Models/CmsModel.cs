﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace develop3r.Models
{
    public class CmsModel
    {
        public int PostId { get; set; }
        public string Tite { get; set; }
        public string Article { get; set; }
        public string Author { get; set; }
        public string Tags { get; set; }
        public DateTime PostedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string UpdatedBy { get; set; }

        public CmsModel(int postId, string title, string article, string author, string tags, DateTime postedOn, DateTime updateOn, string updatedBy)
        {
            PostId = postId;
            Tite = title;
            Article = article;
            Author = author;
            Tags = tags;
            PostedOn = postedOn;
            UpdatedOn = updateOn;
            UpdatedBy = updatedBy;
        }
    }
}