﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace develop3r.Models
{
    public class BlogModel
    {
        public List<CmsModel> CmsModelList { get; set; }
        public List<UserModel> UserModelList { get; set; }

        public BlogModel(List<CmsModel> cmsModelList, List<UserModel> userModelList)
        {
            CmsModelList = cmsModelList;
            UserModelList = userModelList;
        }
    }

}