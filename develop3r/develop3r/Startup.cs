﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(develop3r.Startup))]
namespace develop3r
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
